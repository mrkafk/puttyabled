This directory contains patches against 0.63 PuTTY.

putty_ctrlr.patch:
 - enables Ctrl-R to restart session when the session is broken
 - turns off MessageBox on broken connection, instead printing the message in the title bar
